﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace класс_повторение
{
    public class Person
    {
        public string name;
        public string secondname;
        public int age;
        public int weight;
        public int money;

        public Person()
        {
            Console.WriteLine("Введите свои данные:");
            Console.WriteLine("Введите своё имя");
            name = Console.ReadLine();
            Console.WriteLine("Введите свою фамилию");
            secondname = Console.ReadLine();
            Console.WriteLine("Введите свой возраст");
            age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите свой вес");
            weight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество своих денег");
            money = Convert.ToInt32(Console.ReadLine());
        }
        public void GoAwake()
        {
            Console.WriteLine("Вы пошли");
            weight -= 1;
            Console.WriteLine($"Ваш вес : {weight}");
        }
        public void EatAtHome()
        {
            Console.WriteLine("Вы поели дома");
            weight += 1;
            Console.WriteLine($"Ваш вес : {weight}");
        }
        public void EatAtMak()
        {
            Console.WriteLine("Вы поели в макдональдсе");
            weight += 3;
            Console.WriteLine($"Ваш вес : {weight}");
        }
        public void Life()
        {
            Console.WriteLine("Вы прожили год");
            age += 1;
            Console.WriteLine($"Ваш возраст : {age}");
        }
        public void Job()
        {
            Console.WriteLine("Вы пошли на работу");
            money += 25;
            Console.WriteLine($"Ваши деньги : {money}");
        }
        public void SayInfo()
        {
            Console.WriteLine($"Моё имя: {name}, фамилия: {secondname}");
        }
    }


    class Program
    {
        public static void Menu(Person human)
        {
            Console.WriteLine();
            int a = Convert.ToInt32(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Console.Clear();
                    human.Go();
                    Menu(human);
                    break;
                case 2:
                    Console.Clear();
                    human.Eatathome();
                    Menu(human);
                    break;
                case 3:
                    Console.Clear();
                    human.Eatatmak();
                    Menu(human);
                    break;
                case 4:
                    Console.Clear();
                    human.Life();
                    Menu(human);
                    break;
                case 5:
                    Console.Clear();
                    human.Job();
                    Menu(human);
                    break;
                case 6:
                    Console.Clear();
                    human.Sayinfo();
                    Menu(human);
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Вы ввели неверное число");
                    Menu(human);
                    break;

            }
        }
        static void Main(string[] args)
        {
            Person human = new Person();
            Menu(human);
        }
    }
}
